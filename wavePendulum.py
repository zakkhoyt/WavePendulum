
import math

class WavePendulum:
    def __init__(self, n, l, tmax):
        self.n = n
        self.l = l
        self.tmax = tmax
        self.calculate()

    def calculate(self):
        self.calculateK()
        self.calculateLengths()

    def calculateK(self):
        k = l / 9.8
        k = math.sqrt(k)
        k = k * 2 * math.pi
        k = tmax / k
        k = k - 2
        self.k = k

    def calculateLengths(self):
        lengths = []
        for i in range(n):
            l = self.k + (i + 1) + 1
            l = l * 2.0 * math.pi
            l = tmax / l
            l = math.pow(l, 2)
            l = l * 9.8
            lengths.append(l)
            self.lengths = lengths

    def description(self):
        output = "-------------------------------------------------------------"
        output += "\n" + "Build summary:"
        output += "\n" + "Number of Pendulums: " + str(self.n)
        output += "\n" + "Time of cycle (in seconds): " + str(self.tmax) + " seconds"
        output += "\n" + "Length of first pendulum: " + str(self.l * 1000) + " mm"
        output += "\n" + "Using k value: " + str(self.k)
        output += "\n" + "Pendulum lengths:"

        inchesPerMeter = 39.3701
        for i in range(len(self.lengths)):
            line = "\n\t"
            line += "#" + str(i+1) + ": "

            meters = self.lengths[i]
            mm = meters * 1000
            mmString = "{:.1f}".format(mm) + "mm"
            line += mmString

            inches = meters * inchesPerMeter
            inchesString = "{:.1f}".format(inches) + "\""
            line += " or " + inchesString
            output += line
        return output

print "Calculate pendulum lengths for a wave pendulum"
print "Length is measured from the folcrum of the top to the center of mass of the weight"
print "Inspired by NighthawkInLight"
print "https://www.youtube.com/watch?v=_8JMVl-_KKs"
print ""
print "Please answer three questions:"
print ""

n = int(raw_input('Number of pendulums:'))
l = float(raw_input('Enter length (in millimeters) of first pendulum:')) / 1000
tmax = float(raw_input('Enter time (in seconds) until pattern repeat:'))

# n = 16
# l = 0.23
# tmax = 24

print ""
print "Calculating values for your wave pendulum..."
print ""

pendulum = WavePendulum(n, l, tmax)
print pendulum.description()
