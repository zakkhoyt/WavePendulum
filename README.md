### Setup

* clone this repository
````
git clone git@gitlab.com:zakkhoyt/WavePendulum.git
````
* Navigate to the cloned repository
* Run the script
````
python wavePendulum.py
````
* Answer a few questions to get your values

````
Calculate pendulum lengths for a wave pendulum
Length is measured from the folcrum of the top to the center of mass of the weight
Inspired by NighthawkInLight
https://www.youtube.com/watch?v=_8JMVl-_KKs

Please answer three questions:

Number of pendulums:16
Enter length (in millimeters) of first pendulum:230
Enter time (in seconds) until pattern repeat:24

Calculating values for your wave pendulum...

-------------------------------------------------------------
Build summary:
Number of Pendulums: 16
Time of cycle (in seconds): 24.0 seconds
Length of first pendulum: 230.0 mm
Using k value: 22.9333419477
Pendulum lengths:
	#1: 230.0mm or 9.1"
	#2: 212.6mm or 8.4"
	#3: 197.1mm or 7.8"
	#4: 183.2mm or 7.2"
	#5: 170.8mm or 6.7"
	#6: 159.6mm or 6.3"
	#7: 149.4mm or 5.9"
	#8: 140.2mm or 5.5"
	#9: 131.8mm or 5.2"
	#10: 124.2mm or 4.9"
	#11: 117.2mm or 4.6"
	#12: 110.7mm or 4.4"
	#13: 104.8mm or 4.1"
	#14: 99.4mm or 3.9"
	#15: 94.3mm or 3.7"
	#16: 89.7mm or 3.5"
````

